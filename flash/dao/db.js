/**
 * Created by yishuangxi on 2015/12/23.
 */

var conf = require('../conf');
var mysql = require('mysql');
var pool = mysql.createPool({
    connectionLimit: 10,
    host: conf.mysql.host,
    user: conf.mysql.user,
    password: conf.mysql.password,
    database: conf.mysql.database
});

function query(sql_str, callback){
    pool.getConnection(function (err, conn) {
        if (err) {
            callback(err); return;
        }
        conn.query(sql_str, function (err, rows, fields) {
            callback(err, rows, fields);
            conn.release();
        });
    });
}

module.exports = {
    pool: pool,
    query:query
}