/**
 * Created by yishuangxi on 2016/12/23.
 */

var dao = require('../dao');
var table = require('../conf').mysql.table;
var db = dao.db;
var swig = require('swig');
var mysql = require('mysql');


function queryUserList(username, page, callback) {
    var page_size = 50;
    var start = page_size * (page - 1);
    var sql_str = 'select id, username, realname, phonenum, state, create_time from {{user}} ';
    var sql_values = [];
    //var filter_str = '';
    //if(fields && fields.length){
    //    var field, value, instr_arr = [];
    //    for(var i = 0; i < fields.length; i++){
    //        field = fields[i].field;
    //        value = fields[i].value;
    //        instr_arr.push(' instr({{field}}, "{{value}}") ');
    //    }
    //
    //    filter_str = instr_arr.join(' and ');
    //}
    if(username){
        sql_str += 'instr(username, ?) ';
        sql_values.push(sql_str);
    }
    sql_str += 'limit {{start}}, {{page_size}}';
    sql_str = swig.compile(sql_str)({user: table.user, start: start, page_size: page_size, username:username});
    sql_str = mysql.format(sql_str, sql_values);
    db.query(sql_str, callback);
}

function createUser(username, password, realname, phonenum, state, callback) {
    var sql_str = 'insert into {{user}} VALUES(null, ?, ?, ?, ?, ?, ?)';
    sql_str = swig.compile(sql_str)({user: table.user});
    sql_str = mysql.format(sql_str, [username, password, realname, phonenum, state, parseInt(new Date().getTime() / 1000)]);
    db.query(sql_str, callback);
}

function retrieveUser(id, username, realname, phonenum, state, callback) {
    var sql_str = 'select id, username, realname, phonenum, state, create_time ' +
        'from {{user}} where id=?';
    sql_str = swig.compile(sql_str)({user: table.user});
    sql_str = mysql.format(sql_str, [school_id]);
    db.query(sql_str, callback);
}

function updateUser(id, username, password, realname, phonenum, state, callback) {
    var sql_str = 'update {{user}} as t set ';
    var set_str_arr = [];
    var sql_values = [];
    if (username) {
        set_str_arr.push(' t.username=? ');
        sql_values.push(username);
    }
    if (password) {
        set_str_arr.push(' t.password=? ');
        sql_values.push(password);
    }
    if (realname) {
        set_str_arr.push(' t.realname=? ');
        sql_values.push(realname);
    }
    if (phonenum) {
        set_str_arr.push(' t.phonenum=? ');
        sql_values.push(phonenum);
    }
    if (state) {
        set_str_arr.push(' t.state=? ');
        sql_values.push(state);
    }
    sql_str += set_str_arr.join(',');
    sql_str += ' where id=?';
    sql_values.push(id);
    sql_str = swig.compile(sql_str)({user: table.user});
    sql_str = mysql.format(sql_str, sql_values);
    db.query(sql_str, callback);
}

function deleteUser(id, callback) {
    var sql_str = 'delete from {{user}} where id=?';
    sql_str = swig.compile(sql_str)({user: table.user});
    sql_str = mysql.format(sql_str, [id]);
    db.query(sql_str, callback);
}

module.exports = {
    queryUserList: queryUserList,
    createUser: createUser,
    retrieveUser: retrieveUser,
    updateUser: updateUser,
    deleteUser: deleteUser
}