var express = require('express');
var router = express.Router();
var models = require('../models');

router.get('/user-list.html', function (req, res, next) {
    res.render('user-list.html');
});

router.get('/ajax/user-list', function (req, res, next) {
    var page = parseInt(req.query.page) || 1;
    var username = req.query.username;
    models.user.queryUserList(username, page, function (err, rows, fields) {
        res.json({ret: 0, data: {rows: rows}});
    });
});

router.get('/ajax/user/:id', function (req, res, next) {
    var id = parseInt(req.params.id);
    models.user.queryUserList(page, function (err, rows, fields) {
        res.json({ret: 0, data: {rows: rows}});
    });
});
router.post('/ajax/user:id', function (req, res, next) {
    var id = parseInt(req.params.id);
    models.user.retrieveUser(id, function (err, rows, fields) {
        res.json({ret: 0, data: {rows: rows}});
    });
});
router.put('/ajax/user:id', function (req, res, next) {
    var id = parseInt(req.params.id),
        username = req.query.username,
        password = req.query.password,
        realname = req.query.realname,
        phonenum = req.query.phonenum,
        state = req.query.state;
    models.user.updateUser(id, username, password, realname, phonenum, state, function (err, rows, fields) {
        res.json({ret: 0, data: {rows: rows}});
    });
});

module.exports = router;
