var express = require('express');
var router = express.Router();
var models = require('../models');
router.get('/', function (req, res, next) {
    res.render('home.html');
});
router.get('/welcome.html', function (req, res, next) {
    res.render('welcome.html');
});
module.exports = router;
