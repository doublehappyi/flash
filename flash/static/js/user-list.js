/**
 * Created by yishuangxi on 2015/12/28.
 */
(function($){
    $(function(){
        $('#btn-user-add').click(function(){
            var index = layer.open({
                type:1,
                title:'添加用户',
                area:"700px",
                content:$('#form-user-add')
            });
            layer.load({
                offset:'100px'
            })
            layer.full(index)
        });
    });
})(jQuery);