CREATE DATABASE IF NOT EXISTS flash DEFAULT CHARSET utf8;
use flash;

#用户表
CREATE TABLE IF NOT EXISTS user(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    username VARCHAR(10) UNIQUE NOT NULL,
    password VARCHAR(20) DEFAULT '88888888',
    realname VARCHAR(10) NOT NULL,
    phonenum VARCHAR(11) UNIQUE NOT NULL,
    state    TINYINT(1)   DEFAULT 1, #0禁用， 1启用
    create_time INT(10) NOT NULL
);
ALTER TABLE user auto_increment=1000000000;

#客户表
CREATE TABLE IF NOT EXISTS client(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    name VARCHAR(20) DEFAULT NULL,
    gender   TINYINT(1) DEFAULT 1, #1男，0女
    phonenum VARCHAR(11) UNIQUE DEFAULT NULL,
    interest CHAR(1)   DEFAULT 2, #A,B,C,D
    channel  TINYINT(1) DEFAULT 0,
    user_id INT(10) NOT NULL,
    curr_state   TINYINT(1) NOT NULL, #0战败结束，1成交,2保有，
    create_time INT(10) NOT NULL
);


#汽车表
CREATE TABLE IF NOT EXISTS car(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    cid CHAR(100) UNIQUE NOT NULL,
    model_id INT(10) NOT NULL, #车款
    style_id INT(10) NOT NULL, #车型
    color_id INT(10) NOT NULL, #颜色
    create_time INT(10) NOT NULL
);
ALTER TABLE car auto_increment=10000;
ALTER TABLE car ADD UNIQUE KEY (model_id, style_id, color_id);

#回访表
CREATE TABLE IF NOT EXISTS visit(
    id        INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    content   VARCHAR(200) NOT NULL,
    state_id INT(10) NOT NULL,#当前状态
--    history_state VARCHAR(100) NOT NULL,#历史状态叠加，查询回访表得到
    client_id INT(10) NOT NULL,
    user_id   INT(10) NOT NULL,
    create_time INT(10) NOT NULL
);


#车款表：
CREATE TABLE IF NOT EXISTS model(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    name VARCHAR(20) UNIQUE NOT NULL
);
ALTER TABLE model auto_increment=10000;
INSERT INTO model VALUES(NULL, "S4");
INSERT INTO model VALUES(NULL, "S5");
INSERT INTO model VALUES(NULL, "S6");

#颜色表
CREATE TABLE IF NOT EXISTS color(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    name VARCHAR(20) UNIQUE NOT NULL
);
ALTER TABLE color auto_increment=10000;
INSERT INTO model VALUES(NULL, "黑色");
INSERT INTO model VALUES(NULL, "红色");
INSERT INTO model VALUES(NULL, "白色");



#客户状态表：静态表
CREATE TABLE IF NOT EXISTS state(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    name CHAR(1) UNIQUE NOT NULL
);
ALTER TABLE state auto_increment=10000;
INSERT INTO state VALUES(NULL, "H");#意向最高
INSERT INTO state VALUES(NULL, "A");#意向高
INSERT INTO state VALUES(NULL, "B");#意向中
INSERT INTO state VALUES(NULL, "C");#意向低
INSERT INTO state VALUES(NULL, "O");#订车
INSERT INTO state VALUES(NULL, "F");#战败结束