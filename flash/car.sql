CREATE DATABASE IF NOT EXISTS flash DEFAULT CHARSET utf8;
use flash;
#超级管理员只能有1个，这个人是admin！！！后续如果需要添加普通管理员，则再说！


#营销人员表
CREATE TABLE IF NOT EXISTS manager(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    username VARCHAR(10) UNIQUE NOT NULL,
    password VARCHAR(20) DEFAULT '88888888',
    realname VARCHAR(10) NOT NULL,
    phonenum VARCHAR(11) UNIQUE NOT NULL,
    status   TINYINT(1)   DEFAULT 1, #0禁用， 1启用
    datetime INT(10) NOT NULL
);
ALTER TABLE manager auto_increment=1000000000;

#客户表
CREATE TABLE IF NOT EXISTS client(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    realname VARCHAR(20) DEFAULT NULL,
    gender   TINYINT(1) DEFAULT 1, #1男，0女
    birthday VARCHAR(10) DEFAULT NULL, #YYYY-MM-DD
    phonenum VARCHAR(11) UNIQUE DEFAULT NULL,
    interest CHAR(1)   DEFAULT 2, #A,B,C,D
    manager_id INT(10) NOT NULL,
    result   TINYINT(1) NOT NULL, #0战败结束，1成交,2保有，
    datetime INT(10) NOT NULL
);
ALTER TABLE client auto_increment=1000000000;

#订单表
CREATE TABLE IF NOT EXISTS order(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    car_id  INT(10) NOT NULL,
    status    TINYINT(10) DEFAULT 0, #1已定未交 ，2已交车，3已交现， 4退订， 5退车
    datetime INT(10) NOT NULL
);
ALTER TABLE order auto_increment=1000000000;

#库存表
CREATE TABLE IF NOT EXISTS car(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    brand_id INT(10) NOT NULL,
    model_id INT(10) NOT NULL,
--    style_id INT(10) NOT NULL,
--    displacement_id INT(10) NOT NULL,
    stock   INT(10) NOT NULL,
    datetime INT(10) NOT NULL
);
ALTER TABLE car auto_increment=10000;
ALTER TABLE car ADD UNIQUE KEY (brand_id, model_id, style_id, displacement_id);


#汽车品牌表
CREATE TABLE IF NOT EXISTS brand(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    name VARCHAR(50) UNIQUE NOT NULL,
    datetime INT(10) NOT NULL
);
ALTER TABLE brand auto_increment=10000;
INSERT INTO brand VALUES(NULL, "北汽幻影", 1451206421);
INSERT INTO brand VALUES(NULL, "奔驰", 1451206421);
INSERT INTO brand VALUES(NULL, "宝马", 1451206421);

#汽车型号表
CREATE TABLE IF NOT EXISTS model(
    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
    name VARCHAR(50) UNIQUE NULL,
    brand_id INT(10) NOT NULL,
    datetime INT(10) NOT NULL
);
ALTER TABLE model ADD UNIQUE KEY (name, brand_id);
ALTER TABLE model auto_increment=10000;
INSERT INTO model VALUES(NULL, "AAAAS5", "S5", 1451206421);
INSERT INTO model VALUES(NULL, "AAAAS6", "S6", 1451206421);
INSERT INTO model VALUES(NULL, "AAAAS7", "S7", 1451206421);
INSERT INTO model VALUES(NULL, "AAAAS8", "S8", 1451206421);


--#汽车款型表
--CREATE TABLE IF NOT EXISTS style(
--    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
--    name VARCHAR(50) NOT NULL，
--    datetime INT(10) NOT NULL
--);
--ALTER TABLE model auto_increment=10000;
--INSERT INTO style VALUES(NULL, "舒适型", 1451206421);
--INSERT INTO style VALUES(NULL, "豪华型", 1451206421);
--INSERT INTO style VALUES(NULL, "尊贵型", 1451206421);
--
--
--#排量表
--CREATE TABLE IF NOT EXISTS displacement(
--    id INT(10) UNSIGNED NOT NULL auto_increment PRIMARY KEY,
--    value VARCHAR(20) NOT NULL
--);
--ALTER TABLE model auto_increment=10000;
--INSERT INTO style VALUES(NULL, "1.5L");
--INSERT INTO style VALUES(NULL, "2.0L");
--INSERT INTO style VALUES(NULL, "2.5L");
--INSERT INTO style VALUES(NULL, "1.5T");
--INSERT INTO style VALUES(NULL, "2.0T");
--INSERT INTO style VALUES(NULL, "2.5T");


#营业动作表








